import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';

export default class Avatar extends React.Component {
  
  render() {
    let pic ={
      uri: 'https://lh3.googleusercontent.com/-SMrD9zEB99Q/AAAAAAAAAAI/AAAAAAAAAAA/AGDgw-ijx27kl6b69Zhx8Zxq_WO3fpQTbg/s32-c-mo/photo.jpg'
    };
    return (
      <View style={styles.container}>      
        <Image source={pic} style={styles.image} />        
        <Text style={styles.user}>{this.props.user}</Text> 
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  image: { 
    height: 32,
    width: 32
  },
  user : {    
    paddingLeft: 5,
    fontWeight: 'bold'
  }
});
