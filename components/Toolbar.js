import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

export default class Toolbar extends React.Component {

  state = {
    liked: false
  }

  render() {
    if (this.state.liked) { 
      return (
        <View style={styles.container}>
          <Ionicons size={32} color="red" name="md-heart" />
          <Ionicons size={32} color="black" name="md-cloud-outline" style={styles.icon} />        
          <Ionicons size={32} color="black" name="md-send" />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <Ionicons size={32} color="black" name="md-heart-outline" />
        <Ionicons size={32} color="black" name="md-cloud-outline" style={styles.icon} />        
        <Ionicons size={32} color="black" name="md-send" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  icon: {
    paddingHorizontal: 10
  }
});
