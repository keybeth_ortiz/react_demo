import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default class Counter extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.content}>{this.props.number} Me gusta</Text>        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
  },
  content: {
    fontWeight: 'bold'
  }
});
