import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default class Comment extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.user}>{this.props.user} </Text>
        <Text>{this.props.summary}</Text>        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection:'row'
  },
  user: {
    fontWeight: 'bold'
  }
});
