import React from 'react';
import { StyleSheet, View, Image } from 'react-native';
import Comment from './components/Comment';
import Counter from './components/Counter';
import Toolbar from './components/Toolbar';
import Avatar from './components/Avatar';

export default class App extends React.Component {
  render() {
    let pic = {
      uri: 'https://upload.wikimedia.org/wikipedia/commons/d/de/Bananavarieties.jpg'
    };
    return (      
      <View style={styles.container}>
        <Avatar user='yagquiroz' />
        <Image source={pic} style={styles.image} />
        <Toolbar />
        <Counter number={29} />
        <Comment user='Keybeth' summary='Un comentario' />
      </View>      
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor: '#fff',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'stretch'
  },
  image: { 
    height: 200
  }
});
